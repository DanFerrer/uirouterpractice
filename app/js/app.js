/**
 * Created by Daniel on 10/3/2014.
 */


(function () {

    angular.module('myApp', ['ui.router', 'ngAnimate'])
        .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('program');
            $stateProvider
                .state('program', {
                    url: '/program',
                    templateUrl: 'partials/program.html',
                    controller: function($scope) {
                        $scope.program = {
                            name: 'Program 1',
                            description: 'This is a program'
                        };
                        console.log('Exercise Program is good!!')
                    }
                })
                .state('session', {
                    url: '/session',
                    templateUrl: 'partials/session.html'
                });
                console.log("Exercise Session is good!!");
        }]);

})();
