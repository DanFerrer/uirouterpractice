/**
 * Created by Daniel on 10/3/2014.
 */

angular.module('wizardApp', ['ui.router','ngAnimate','wizardapp.controllers'])
    .config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/wizard/start');
            $stateProvider
                .state('wizard', {
                    abstract: true,
                    url: '/wizard',
                    template: '<div><div ui-view></div></div>'
                })
                .state('wizard.start', {
                    url: '/start',
                    templateUrl: 'wizard/step_1.html'
                })
                .state('wizard.email', {
                    url: '/email',
                    templateUrl: 'wizard/step_2.html'
                })
                .state('wizard.finish', {
                    url: '/finish',
                    templateUrl: 'wizard/step_3.html',
                    controller: function($scope) {
                        $scope.signup();
                    }
                });
        }]);



